import React, {Component} from "react";
import CardList from "./CardList";

class App extends Component{
    constructor(){
        super()
        this.state = {
            robots: [],
            searchfield: []
        }
    }

    componentDidMount(){
        fetch("https://api.openbrewerydb.org/breweries")
            .then(response => response.json())
            .then(cervecerias => this.setState({robots: cervecerias}));
    }
    
    onSearchChange = (event) => {
        fetch(`https://api.openbrewerydb.org/breweries/${this.state.robots[9].id}`)
            .then(response => response.json())
            .then(cerveceria => this.setState({robots: cerveceria}));
            console.log("click")
    }


    render(){
        return (
            <div className="tc">
                <h1>Cervecerias</h1>
                <CardList
                     searchChange={this.onSearchChange} 
                     robots={this.state.robots} 
                />
            </div>
        )
    }
}

export default App;